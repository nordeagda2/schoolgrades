package com.amen.classes;

public class Grade {
	private Float grade;
	private GradeType type;

	public Float getGrade() {
		return grade;
	}

	public void setGrade(Float grade) {
		this.grade = grade;
	}

	public Grade(Float grade, GradeType type) {
		super();
		this.grade = grade;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Grade [grade=" + grade + ", type=" + type + "]";
	}

}
