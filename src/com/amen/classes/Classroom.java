package com.amen.classes;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Classroom {
	//new TreeSet<String>(Comparator.comparing(Integer::valueOf));
	private List<Student> students = new LinkedList <Student>();
	private Map<SubjectType, SchoolSubject> subjects = new HashMap<>();
	
	public void addStudent(Student s){
		System.out.println("Dodaje studenta: " + s);
		students.add(s);
	}
	
	public void addSubject(SubjectType type){
		subjects.put(type, new SchoolSubject(students.size()));
	}
	
	public void printAllStudents(){	
		Collections.sort(students, new Comparator<Student>() {
		    public int compare(Student o1, Student o2) {
		        return o1.getName().compareTo(o2.getName());
		    }
		});
		
		System.out.println("Studenci: ");
		for(Student s : students){
			System.out.println(s);
		}
	}
	
	public void addGrade(Integer studentId, SubjectType type, Grade grade){
		System.out.println("Dodaje ocene uczniowi o id -> " +studentId + " z przedmiotu: " + type.toString() + ", ocena: " + grade.toString());
		subjects.get(type).addGrade(studentId, grade);
	}
	
}
